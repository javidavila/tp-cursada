let Producto = require("./Producto");
let CarritoCompras = require("./CarritoCompras");

let producto1, producto2, producto3, carrito1, carrito2;

beforeEach(() => {
    producto1 = new Producto("Mouse", 500);
    producto2 = new Producto("Procesador Intel", 20000);
    producto3 = new Producto("HDD", 3000);

    carrito1 = new CarritoCompras();
    carrito2 = new CarritoCompras();

})

test("Test de agregar productos a carrito", () => {
    expect(carrito1.consultarProductos().length).toBe(0);
    carrito1.agregarProductos(producto1);
    expect(carrito1.consultarProductos()).toContain(producto1);
    carrito1.agregarProductos(producto2);
    carrito1.agregarProductos(producto3);
    expect(carrito1.consultarProductos()).toContain(producto3);
    expect(carrito1.consultarProductos().length).toBe(3);
})

test("Test de consultar total", () => {
    carrito2.agregarProductos(producto1);
    carrito2.agregarProductos(producto3);
    expect(carrito2.consultarProductos()).toContain(producto3);
    expect(carrito2.consultarTotal()).toBe(3500);
})