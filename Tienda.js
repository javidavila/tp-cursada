const Venta = require("./Venta");
const CarritoCompras = require("./CarritoCompras");
const Suscripcion = require("./Suscripcion");
const SuscripcionPorCantidad = require("./SuscripcionPorCantidad");
const SuscripcionPorVenta = require("./SuscripcionPorVenta");

class Tienda {

    constructor() {
        this.suscripcion = new Suscripcion();
        this.ventas = [];
        this.contadorVentas = {};
        this.productosVendidos = {};
    }

    registrarSuscripcionPorVenta(porcentaje) {
        this.suscripcion = new SuscripcionPorVenta(porcentaje);
    }

    registrarSuscripcionPorCantidad(abono, cantidadLimiteVentas, costoAdicional) {
        this.suscripcion = new SuscripcionPorCantidad(abono, cantidadLimiteVentas, costoAdicional);
    }

    consultarMontoDeSuscripcion() {
        return this.suscripcion.consultarMontoTotal(this)
    }

    pagarSuscripcion(monto) {
        let saldo = this.suscripcion.pagarSuscripcion(monto);
        return saldo;
    }

    consultarVentas() {
        return this.ventas;
    }

    agregarVenta(venta) {
        this.ventas.push(venta);
        venta.consultarProductos().forEach(producto => {
            if (!(producto.consultarNombre() in this.contadorVentas)) {
                this.contadorVentas[producto.consultarNombre()] = 0;
                this.productosVendidos[producto.consultarNombre()] = producto;
            }
            this.contadorVentas[producto.consultarNombre()] = this.contadorVentas[producto.consultarNombre()] + 1;
        });
    }

}

module.exports = Tienda;