const CarritoCompras = require("./CarritoCompras");
class Venta {
    constructor(carrito, cliente) {
        this.carrito = carrito;
        this.cliente = cliente;
        this.estado = new Recibida();
        this.envio = new FormaEnvio();
        this.fechaDePago;
        this.fechaDeRetiro;
    }

    volverRecibida() {
        this.estado.recibir()
        venta.carrito.consultarProductos().forEach(producto => {
            producto.devolverStock();
        });
        return;
    }

    cancelarPedido() {
        this.estado.cancelar()
        return;
    }

    aceptarPedido() {
        this.estado.aceptar()
        this.carrito.consultarProductos().forEach(producto => {
            producto.descontarStock();
        });
        return;
    }

    entregarPedido() {
        this.estado.entregar()
        return;
    }

    consultarEstado() {
        return this.estado;
    }

    consultarCarrito() {
        return this.carrito;
    }

    consultarProductos() {
        return this.carrito.consultarProductos();
    }

    consultarFechaDePago() {
        return this.fechaDePago;
    }

    registrarFechaDeRetiro(fechaDeRetiro) {
        this.fechaDeRetiro = fechaDeRetiro;
    }

    registrarPago() {
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        this.fechaDePago = new Date(date + ' ' + time);
        return;
    }

    consultarMontoDeEnvio() {
        return this.envio.obtenerMontoDeEnvio()
    }

    consultaFechaDeEntrega() {
        return this.envio.obtenerFechaDeEntrega(this);
    }

    registrarRetiroEnSucursal() {
        this.envio = new RetiroSucursal();
    }

    registrarEntregaInmediata() {
        this.envio = new EntregaInmediata();
    }

    registrarEnvioTradicional() {
        this.envio = new EnvioTradicional();
    }

    consultarMontoTotal(){
        return (this.envio.obtenerMontoDeEnvio() + this.carrito.consultarTotal());
    }

}

class EstadoVenta {
    cancelar() { }
    aceptar() { }
    recibir() { }
    entregar() { }
}

class Recibida extends EstadoVenta {
    cancelar(venta) {
        venta.cambiarEstado(new Cancelada())
    }
    aceptar(venta) {
        venta.cambiarEstado(new Aceptada())
    }
}

class Aceptada extends EstadoVenta {
    recibir(venta) {
        venta.cambiarEstado(new Recibida())
    }

    entregar(venta) {
        venta.cambiarEstado(new Entregada())
    }
}
class Entregada extends EstadoVenta {
    cambiarEstado() { }
}
class Cancelada extends EstadoVenta {
    cambiarEstado() { }
}

class FormaEnvio {
    obtenerMontoDeEnvio() { }
    obtenerFechaDeEntrega() { }
}

class RetiroSucursal extends FormaEnvio {

    obtenerFechaDeEntrega(venta) {
        return venta.fechaDeRetiro;
    }
    obtenerMontoDeEnvio() {
        return 0;
    }
}
class EnvioTradicional extends FormaEnvio {

    obtenerFechaDeEntrega(venta) {
        return new Date(venta.fechaDePago.getFullYear(), venta.fechaDePago.getMonth(), venta.fechaDePago.getDate() + 4)
    }
    obtenerMontoDeEnvio() {
        return 450;
    }
}
class EntregaInmediata extends FormaEnvio {
    obtenerFechaDeEntrega(venta) {
        return new Date(venta.fechaDePago.getFullYear(), venta.fechaDePago.getMonth(), venta.fechaDePago.getDate() + 1);
    }
    obtenerMontoDeEnvio() {
        return 700;
    }

}

module.exports = Venta;