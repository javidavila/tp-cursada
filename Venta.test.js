let Venta = require("./Venta");
let Producto = require("./Producto");
let CarritoCompras = require("./CarritoCompras");
let Tienda = require("./Tienda");

let producto1, producto2, producto3, carrito1, carrito2, venta1, venta2, tienda;

beforeEach(() => {
    producto1 = new Producto("Mouse", 500);
    producto2 = new Producto("Procesador Intel", 20000);
    producto3 = new Producto("HDD", 3000);

    carrito1 = new CarritoCompras();
    carrito2 = new CarritoCompras();

    venta1 = new Venta(carrito1, 1)
    venta2 = new Venta(carrito2, 1)

    tienda = new Tienda()

})

test("Test de venta1", () => {

    carrito1.agregarProductos(producto1);
    carrito1.agregarProductos(producto2);
    carrito1.agregarProductos(producto3);

    venta1.registrarEnvioTradicional()
    expect(venta1.consultarMontoDeEnvio()).toBe(450);
    venta1.registrarPago()

    var fechaDePago = venta1.consultarFechaDePago();
    var fechaDeEntrega = new Date(fechaDePago.getFullYear(), fechaDePago.getMonth(), fechaDePago.getDate() + 4);

    expect(venta1.consultaFechaDeEntrega().getDate()).toBe(fechaDeEntrega.getDate());
    expect(venta1.consultaFechaDeEntrega().getMonth()).toBe(fechaDeEntrega.getMonth());

    expect(venta1.consultarMontoTotal()).toBe(23950);

})

test("Test de venta2", () => {

    var fechaDeRetiro = new Date("2021-09-01 12:00:00");
    carrito2.agregarProductos(producto3);

    venta2.registrarRetiroEnSucursal()
    expect(venta2.consultarMontoDeEnvio()).toBe(0);
    venta2.registrarFechaDeRetiro(fechaDeRetiro)

    expect(venta2.consultaFechaDeEntrega().getDate()).toBe(fechaDeRetiro.getDate());
    expect(venta2.consultaFechaDeEntrega().getMonth()).toBe(fechaDeRetiro.getMonth());

    expect(venta2.consultarMontoDeEnvio()).toBe(0);

    expect(venta2.consultarMontoTotal()).toBe(3000);

})

