class Producto{
    constructor(nombre="",precio=0){
        this.nombre = nombre;
        this.precio = precio;
    }
    consultarPrecio(){
        return this.precio;
    }
    consultarNombre(){
        return this.nombre;
    }
}

module.exports = Producto;