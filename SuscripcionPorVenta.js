const Suscripcion = require("./Suscripcion");

class SuscripcionPorVenta extends Suscripcion {
    constructor(porcentaje) {
        super();
        this.porcentaje = porcentaje;
    }

    consultarMontoTotal(tienda) {
        let total = 0;
        tienda.ventas.forEach(venta => {
            total += venta.consultarMontoTotal()
        });
        return total * (this.porcentaje / 100);
    }

    consultarPorcentaje() {
        return this.porcentaje;
    }

    pagarSuscripcion(monto) {
        let saldo = this.consultarMontoTotal() - monto;
        return saldo;
    }
}

module.exports = SuscripcionPorVenta;