let Venta = require("./Venta");
let Producto = require("./Producto");
let CarritoCompras = require("./CarritoCompras");
let Tienda = require("./Tienda");

let producto1, producto2, producto3, carrito, venta, tienda;

beforeEach(() => {
    producto1 = new Producto("Teclado", 500);
    producto2 = new Producto("Gabinete", 1000);
    producto3 = new Producto("Monitor", 5000);

    carrito = new CarritoCompras();

    carrito.agregarProductos(producto1);
    carrito.agregarProductos(producto2);
    carrito.agregarProductos(producto3);

    venta = new Venta(carrito, 1)

    venta.registrarEnvioTradicional()
    venta.registrarPago()

    tienda = new Tienda()

})

test("Test de tienda 1", () => {

    tienda.registrarSuscripcionPorVenta(5)
    tienda.agregarVenta(venta);
    expect(tienda.consultarVentas().length).toBe(1);
    expect(tienda.consultarMontoDeSuscripcion()).toBe(347.5);

})


test("Test de tienda 2", () => {

    tienda.registrarSuscripcionPorCantidad(3000, 1, 100)
    tienda.agregarVenta(venta);
    tienda.agregarVenta(venta);
    tienda.agregarVenta(venta);
    expect(tienda.consultarVentas().length).toBe(3);
    expect(tienda.consultarMontoDeSuscripcion()).toBe(3200);

})

