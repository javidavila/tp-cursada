const Producto = require("./Producto");

class VarianteProducto extends Producto {
    constructor(tamanio = 0, color = "", material = "", stock = 0
    ) {
        this.tamanio = tamanio;
        this.color = color;
        this.material = material;
        this.stock = stock;
    }
    consultarTamanio() {
        return this.tamanio;
    }
    consultarColor() {
        return this.color;
    }
    consultarMaterial() {
        return this.material;
    }
    consultarStock() {
        return this.stock;
    }

}

module.exports = VarianteProducto;