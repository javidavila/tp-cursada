const VarianteProducto = require("./VarianteProducto");

class CarritoComprasProducto extends VarianteProducto {
    constructor(cantidad = 0) {
        this.cantidad = cantidad;
    }
    consultarCantidad() {
        return this.cantidad;
    }

    descontarStock() {
        this.stock -= this.cantidad;
        return;
    }

    devolverStock() {
        this.stock += this.cantidad;
        return;
    }
}

module.exports = CarritoComprasProducto;