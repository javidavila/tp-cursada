
class EstadoVenta {
    constructor() {

    }
    cambiarEstado(estado) {

        if (estado == "Entregada" && estado == "Cancelada")
            return "Pedido entregado";

        if (estado == "Cancelada")
            return "No se puede cambiar el estado";

        if (this.estado == "Aceptada" && estado == "Recibida")
            this.cart.consultarProductos().forEach(producto => {
                producto.devolverStock();
            });

        this.estado = estado;

        return "Estado cambiado a: " + estado;
    }
}

class Recibida extends EstadoVenta{
    
}
class Entregada extends EstadoVenta{

}
class Cancelada extends EstadoVenta{
    
}
class Aceptada extends EstadoVenta{

}



module.exports = EstadoVenta;