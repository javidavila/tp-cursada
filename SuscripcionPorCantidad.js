const Tienda = require("./Tienda");
const Suscripcion = require("./Suscripcion");

class SuscripcionPorCantidad extends Suscripcion {
    constructor(abono, cantidadLimiteVentas, costoAdicional) {
        super();
        this.abono = abono;
        this.cantidadLimiteVentas = cantidadLimiteVentas;
        this.costoAdicional = costoAdicional;
    }

    consultarMontoTotal(tienda) {
        if (tienda.consultarVentas().length <= this.cantidadLimiteVentas) {
            return this.abono;
        } else {
            return (this.abono + ((tienda.consultarVentas().length - this.cantidadLimiteVentas) * this.consultarAdicionalVentas()));
        }
    }

    consultarCantidadLimiteVentas() {
        return this.cantidadLimiteVentas;
    }

    consultarAdicionalVentas() {
        return this.costoAdicional;
    }

    pagarSuscripcion(monto) {
        let total = this.consultarMontoAbono() + this.consultarAdicionalVentas();
        let saldo = total - monto;

        return saldo;
    }
}

module.exports = SuscripcionPorCantidad;