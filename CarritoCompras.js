let Producto = require("./Producto.js");

class CarritoCompras{
    constructor(){
        this.productos = [];
    }

    consultarProductos(){
        return this.productos;
    }

    consultarCantidadProductos(){
        return this.productos.length;
    }

    agregarProductos(producto){
        this.productos.push(producto);
    }

    quitarProductos(productoA){
        if(!this.productos.includes(productoA)){
            throw("El producto no está en el carrito");
        }
        this.productos = this.productos.filter((producto)=>{return producto != productoA})
    }

    consultarTotal(){
        let total = 0;
        this.productos.forEach(producto=>{
            total = total + producto.consultarPrecio();
        })
        return total;
    }

}

module.exports = CarritoCompras;